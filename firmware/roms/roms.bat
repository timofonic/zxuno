GenRom 0    8  4 0 0  "'ZX Spectrum 48K Cargando Leches'" leches.rom         leches.tap
GenRom tdn  8  4 0 0  "'ZX +3e DivMMC'"                   mmces3eE.rom       mmces3eE.tap
GenRom d    10 2 4 0  "'SE Basic IV 4.0 Anya'"            se.rom             se.tap
GenRom 0    11 1 4 48 "'ZX Spectrum 48K'"                 48.rom             48.tap
GenRom 0    11 1 4 48 "'Jet Pac (1983)'"                  JetPac.rom         JetPac.tap
GenRom 0    11 1 4 48 "'Pssst (1983)'"                    Pssst.rom          Pssst.tap
GenRom 0    11 1 4 48 "'Cookie (1983)'"                   Cookie.rom         Cookie.tap
GenRom 0    11 1 4 48 "'Tranz Am (1983)'"                 TranzAm.rom        TranzAm.tap
GenRom 0    11 1 4 48 "'Master Chess (1983)'"             MasterChess.rom    MasterChess.tap
GenRom 0    11 1 4 48 "'Backgammon (1983)'"               Backgammon.rom     Backgammon.tap
GenRom 0    11 1 4 48 "'Hungry Horace (1983)'"            HungryHorace.rom   HungryHorace.tap
GenRom 0    11 1 4 48 "'Horace & the Spiders (1983)'"     HoraceSpiders.rom  HoraceSpiders.tap
GenRom 0    11 1 4 48 "'Planetoids (1983)'"               Planetoids.rom     Planetoids.tap
GenRom 0    11 1 4 48 "'Space Raiders (1983)'"            SpaceRaiders.rom   SpaceRaiders.tap
GenRom 0    11 1 4 48 "'Deathchase (1983)'"               Deathchase.rom     Deathchase.tap
GenRom 0    11 1 4 48 "'Manic Miner (1983)'"              ManicMiner.rom     ManicMiner.tap
GenRom 0    11 1 4 48 "'Misco Jones (2013)'"              MiscoJones.rom     MiscoJones.tap
